
-- Theme edited for personal use by C.D.M. Rørmose, based on the "dark.lua" theme
-- ORIGINAL COPYRIGHT INFORMATION FOR "dark.lua" IS AS FOLLOWS:
-------------------------------------------------------------------------
-- Copyright 2007-2019 Mitchell mitchell.att.foicica.com. See LICENSE. --
-- Dark theme for Textadept.                                           --
-- Contributions by Ana Balan.                                         --
-------------------------------------------------------------------------

local buffer = buffer
local property, property_int = buffer.property, buffer.property_int

-- Greyscale colors.
--property['color.dark_black'] = 0x000000
property['color.black'] = 0x000000
property['color.light_black'] = 0x202020
property['color.grey_black'] = 0x4D4D4D
property['color.dark_grey'] = 0x666666
property['color.grey'] = 0xAAAAAA
property['color.light_grey'] = 0xD8D8D8
property['color.grey_white'] = 0xB3B3B3
--property['color.dark_white'] = 0xEEEEEE
property['color.white'] = 0xE6E6E6
property['color.light_white'] = 0xFFFFFF

-- Dark colors.
property['color.dark_red'] = 0x000066
property['color.dark_yellow'] = 0x1A6666
property['color.dark_green'] = 0x008100
--property['color.dark_teal'] = 0x66661A
property['color.dark_purple'] = 0x551A66
property['color.dark_orange'] = 0x1A5577
property['color.dark_pink'] = 0x990099
--property['color.dark_lavender'] = 0xB36666
property['color.dark_blue'] = 0x810000

-- Medium colors.
property['color.blue'] = 0xFF0000
property['color.green'] = 0x00FF00
property['color.lavender'] = 0xDD99E6
property['color.orange'] = 0x05A9FF
property['color.pink'] = 0xFF00FF
property['color.purple'] = 0xCC4DEE
property['color.red'] = 0x0000FF
property['color.teal'] = 0xCCAA55
property['color.yellow'] = 0x04FAFF

-- Light colors.
property['color.light_red'] = 0x9999FF
property['color.light_yellow'] = 0x77FFFF
property['color.light_green'] = 0x66FF66
--property['color.light_teal'] = 0xCCCC80
--property['color.light_purple'] = 0xCC80CC
property['color.light_orange'] = 0x66BBFF
property['color.light_pink'] = 0xFF80FF
--property['color.light_lavender'] = 0xFFCCCC
property['color.light_blue'] = 0xFF8888

-- Default font.
property['font'], property['fontsize'] = 'ProggyCleanTTSZBP', 12
--if WIN32 then
--  property['font'] = 'Courier New'
--elseif OSX then
--  property['font'], property['fontsize'] = 'Monaco', 12
--end

-- Predefined styles.
property['style.default'] = 'font:$(font),size:$(fontsize),'..
                            'fore:$(color.orange),back:$(color.light_black)'
property['style.line_number'] = 'fore:$(color.red),back:$(color.black)'
--property['style.controlchar'] =
property['style.indentguide'] = 'fore:$(color.black),back:$(color.blue)'
property['style.calltip'] = 'fore:$(color.light_grey),back:$(color.black)'
property['style.folddisplaytext'] = 'fore:$(color.red),back:$(color.black)'

-- Token styles.
property['style.class'] = 'fore:$(color.light_yellow),back:$(color.black)'
property['style.comment'] = 'fore:$(color.grey),back:$(color.black)'
property['style.constant'] = 'fore:$(color.green),back:$(color.black)'
property['style.embedded'] = '$(style.keyword),back:$(color.black)'
property['style.error'] = 'fore:$(color.green),back:$(color.white),italics'
property['style.function'] = 'fore:$(color.white),back:$(color.dark_red)'
property['style.identifier'] = ''
property['style.keyword'] = 'fore:$(color.yellow),back:$(color.dark_red)'
property['style.label'] = 'fore:$(color.orange),back:$(color.black)'
property['style.number'] = 'fore:$(color.light_pink),back:$(color.dark_blue)'
property['style.operator'] = 'fore:$(color.red),back:$(color.black),bold'
property['style.preprocessor'] = 'fore:$(color.pink),back:$(color.black)'
property['style.regex'] = 'fore:$(color.light_yellow),back:$(color.black)'
property['style.string'] = 'fore:$(color.light_yellow),back:$(color.red)'
property['style.type'] = 'fore:$(color.green),back:$(color.black)'
property['style.variable'] = 'fore:$(color.light_green),back:$(color.black)'
property['style.whitespace'] = 'back:$(color.black)'

-- Multiple Selection and Virtual Space
--buffer.additional_sel_alpha =
--buffer.additional_sel_fore =
--buffer.additional_sel_back =
--buffer.additional_caret_fore =

-- Caret and Selection Styles.
buffer:set_sel_fore(true, property_int['color.black'])
buffer:set_sel_back(true, property_int['color.purple'])
--buffer.sel_alpha =
buffer.caret_fore = property_int['color.red']
buffer.caret_line_back = property_int['color.blue']
-- buffer.caret_line_back_alpha = 127

-- Fold Margin.
buffer:set_fold_margin_color(true, property_int['color.red'])
buffer:set_fold_margin_hi_color(true, property_int['color.red'])

-- Markers.
local MARK_BOOKMARK = textadept.bookmarks.MARK_BOOKMARK
--buffer.marker_fore[MARK_BOOKMARK] = property_int['color.black']
buffer.marker_back[MARK_BOOKMARK] = property_int['color.dark_green']
--buffer.marker_fore[textadept.run.MARK_WARNING] = property_int['color.black']
buffer.marker_back[textadept.run.MARK_WARNING] = property_int['color.yellow']
--buffer.marker_fore[textadept.run.MARK_ERROR] = property_int['color.black']
buffer.marker_back[textadept.run.MARK_ERROR] = property_int['color.red']
for i = 25, 31 do -- fold margin markers
  buffer.marker_fore[i] = property_int['color.light_orange']
  buffer.marker_back[i] = property_int['color.black']
  buffer.marker_back_selected[i] = property_int['color.light_grey']
end

-- Indicators.
buffer.indic_fore[ui.find.INDIC_FIND] = property_int['color.dark_yellow']
buffer.indic_alpha[ui.find.INDIC_FIND] = 255
local INDIC_BRACEMATCH = textadept.editing.INDIC_BRACEMATCH
buffer.indic_fore[INDIC_BRACEMATCH] = property_int['color.light_grey']
local INDIC_HIGHLIGHT = textadept.editing.INDIC_HIGHLIGHT
buffer.indic_fore[INDIC_HIGHLIGHT] = property_int['color.orange']
buffer.indic_alpha[INDIC_HIGHLIGHT] = 255
local INDIC_PLACEHOLDER = textadept.snippets.INDIC_PLACEHOLDER
buffer.indic_fore[INDIC_PLACEHOLDER] = property_int['color.grey']

-- Call tips.
--buffer.call_tip_fore_hlt = property_int['color.light_blue']

-- Long Lines.
buffer.edge_color = property_int['color.dark_grey']
